package main

// this is a comment

import (
  "crypto/hmac"
  "crypto/sha256"
  "encoding/hex"
  "fmt"
  "net/http"
  "strings"
  "log"
  "os"
  "os/exec"
  "regexp"
  "time"
  "io/ioutil"
  "github.com/go-telegram-bot-api/telegram-bot-api"
  "github.com/tidwall/gjson"
  "github.com/sethvargo/go-password/password"
)


func buildSignature(body, secret []byte) (string, error) {
  hash := hmac.New(sha256.New, secret)
  payload := append([]byte("v0:"), body...)

  if _, err := hash.Write(payload); err != nil {
    return "", err
  }

  src := hash.Sum(nil)

  dst := make([]byte, hex.EncodedLen(len(src)))
  hex.Encode(dst, src)

  return "v0=" + string(dst), nil
}

func send_to_joe(in_json, in_type, in_path string) (string, error) {
  //_json := `{"session_id":"webui-i182","command_id":"579","text":"explain select 1","channel_id":"ProductionDB","user_id":"15","timestamp":"2020-05-07 11:15:28.788146"}`
  _json := in_json
  _body := []byte(_json)
  _secret := []byte("2")
  a, _ := buildSignature(_body, _secret)
  fmt.Println(a)

  _jsonobj := strings.NewReader(_json)
  req, _ := http.NewRequest(in_type,"http://127.0.0.1:2400"+in_path,_jsonobj)
  req.Header.Add("accept", `application/json`)
  req.Header.Add("Verification-Signature",a)
  client := &http.Client{}
  resp, err := client.Do(req)
  defer resp.Body.Close()
  fmt.Println(req)
  fmt.Println(resp)
  bodyBytes, err := ioutil.ReadAll(resp.Body)
  check(err)
  bodyString := string(bodyBytes)
  return bodyString,err
}

func send_to_dblab(in_json, in_type, in_path string) (string, error) {
  _json := in_json
  //_body := []byte(_json)

  _jsonobj := strings.NewReader(_json)
  req, _ := http.NewRequest(in_type,"http://127.0.0.1:2345"+in_path,_jsonobj)
  req.Header.Add("accept", `application/json`)
  req.Header.Add("Verification-Token","secretkjf33tolsk")
  req.Header.Add("Content-Type", `application/json`)
  client := &http.Client{}
  resp, err := client.Do(req)
  defer resp.Body.Close()
  fmt.Println(req)
  fmt.Println(resp)
  bodyBytes, err := ioutil.ReadAll(resp.Body)
  check(err)
  bodyString := string(bodyBytes)
  return bodyString,err
}

func main() {
telega()
}

func check(err error) {
    if err != nil {
        fmt.Println(err.Error())
        os.Exit(1)
    }
}

func telega() {
	bot, err := tgbotapi.NewBotAPI("1214023530:AAGpuHfWmBvfCHiItzjE4ietDmIc7GUSWFE")
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
		s := strings.Fields(strings.Replace(update.Message.Text, "/", "", 1))
                //each iteration generate new password
                _password, _ := password.Generate(10, 3, 0, false, false)

                pattern := `^/exec.+$`
                matched, err := regexp.Match(pattern, []byte(update.Message.Text))
                check(err)
                if matched {
                  _json := `{"session_id":"webui-i182","command_id":"579","text":"explain select 1","channel_id":"ProductionDB","user_id":"15","timestamp":"2020-05-07 11:15:28.788146"}`
                  _path := `/webui/command`
                  _resp,err := send_to_joe(_json,`POST`,_path)
                  check(err)
                  msg := tgbotapi.NewMessage(update.Message.Chat.ID,`A: `+_resp)
                  msg.ReplyToMessageID = update.Message.MessageID
                  bot.Send(msg)
                }
                pattern = `^/status_joe$`
                matched, err = regexp.Match(pattern, []byte(update.Message.Text))
                check(err)
                if matched {
                  _json := `{}`
                  _path := `/`
                  _resp,err := send_to_joe(_json,`GET`,_path)
                  check(err)
                  msg := tgbotapi.NewMessage(update.Message.Chat.ID,`A: `+_resp)
                  msg.ReplyToMessageID = update.Message.MessageID
                  bot.Send(msg)
                }
                pattern = `^/channels_joe$`
                matched, err = regexp.Match(pattern, []byte(update.Message.Text))
                check(err)
                if matched {
                  _json := `{}`
                  _path := `/webui/channels`
                  _resp,err := send_to_joe(_json,`GET`,_path)
                  check(err)
                  msg := tgbotapi.NewMessage(update.Message.Chat.ID,`A: `+_resp)
                  msg.ReplyToMessageID = update.Message.MessageID
                  bot.Send(msg)
                }
                pattern = `^/snapshots_joe$`
                matched, err = regexp.Match(pattern, []byte(update.Message.Text))
                check(err)
                if matched {
                  _json := `{"challenge":"list"}`
                  _path := `/webui/verify`
                  _resp,err := send_to_joe(_json,`POST`,_path)
                  check(err)
                  msg := tgbotapi.NewMessage(update.Message.Chat.ID,`A: `+_resp)
                  msg.ReplyToMessageID = update.Message.MessageID
                  bot.Send(msg)
                }
		//output = subprocess.check_output(" /usr/local/bin/dblab clone create --username " + login + " --password " + password, shell=True)
		//curl -X POST "http://gitlab.com/clone" -H "accept: application/json" -H "Verification-Token: 123" -H "Content-Type: application/json" -d "{ \"id\": \"string\", \"snapshot\": { \"id\": \"string\" }, \"protected\": false, \"db\": { \"username\": \"string\", \"password\": \"string\" }}"

                pattern = `^/clone\ .*$`
                matched, err = regexp.Match(pattern, []byte(update.Message.Text))
                check(err)
		//if snapshot not given, take latest
		_snaplast := ""
		if len(s) < 3 {
			_snaps,err := send_to_dblab(`{}`,`GET`,`/snapshots`)
			check(err)
			_snaplast = gjson.Get(_snaps, "0.id").String()
		} else { _snaplast = s[2] }
		//if pattern match string then
                if matched {
		  if len(s) < 2 {
                          msg := tgbotapi.NewMessage(update.Message.Chat.ID,`A: 
			  min 1 params
			  usage: 
			   - clone <dbname or any name> <id of /snapshots command>
			  `)
			  msg.ReplyToMessageID = update.Message.MessageID
			  bot.Send(msg)
		  } else {
		  _json := `{"id":"`+s[1]+`","snapshot":{"id":"`+_snaplast+`"},"protected":false,"db":{"restricted":false,"username":"`+s[1]+`_tmp","password":"`+_password+`","db_name": "bp_lab"}}`
                  _path := `/`+s[0]
                  _resp,err := send_to_dblab(_json,`POST`,_path)
                  check(err)
		  if err == nil {
		  _status := gjson.Get(_resp, "status.code")
		  _id := gjson.Get(_resp, "id")
		  pattern = `^(OK|FATAL|ERROR)$`
		  matched, err = regexp.Match(pattern, []byte(_status.String()))
		  for {
			_path := `/clone/`+_id.String()
			_json := `{}`
			_resp,err = send_to_dblab(_json,`GET`,_path)
			_status = gjson.Get(_resp, "status.code")
			matched, err = regexp.Match(pattern, []byte(_status.String()))
			if matched { break }
			time.Sleep(2 * time.Second)
		  }
}
		//Forbidden: bot can't initiate conversation with a user
		//_privmsg := tgbotapi.NewMessage(int64(update.Message.From.ID), _password)
		  msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Password for clone "+s[1]+": "+_password)
                  msg.ReplyToMessageID = update.Message.MessageID
                  bot.Send(msg)
                  msg = tgbotapi.NewMessage(update.Message.Chat.ID,`A: `+_resp)
                  msg.ReplyToMessageID = update.Message.MessageID
                  bot.Send(msg)
	          }
                }
                pattern = `^/(snapshots|status)$`
                matched, err = regexp.Match(pattern, []byte(update.Message.Text))
                check(err)
                if matched {
		  _json := `{}`
                  _path := `/`+s[0]
                  _resp,err := send_to_dblab(_json,`GET`,_path)
                  check(err)
                  msg := tgbotapi.NewMessage(update.Message.Chat.ID,`A: `+_resp)
                  msg.ReplyToMessageID = update.Message.MessageID
                  bot.Send(msg)
                }
                pattern = `^/clone_status\ .*$`
                matched, err = regexp.Match(pattern, []byte(update.Message.Text))
                check(err)
                if matched {
			_json := `{}`
			_path := `/clone/`+s[1]
                  _resp,err := send_to_dblab(_json,`GET`,_path)
                  check(err)
                  msg := tgbotapi.NewMessage(update.Message.Chat.ID,`A: `+_resp)
                  msg.ReplyToMessageID = update.Message.MessageID
                  bot.Send(msg)
                }
                pattern = `^/clone_delete\ .*$`
                matched, err = regexp.Match(pattern, []byte(update.Message.Text))
                check(err)
                if matched {
		  _json := `{}`
		  _path := `/clone/`+s[1]
                  _resp,err := send_to_dblab(_json,`DELETE`,_path)
                  check(err)
		  app := "/usr/bin/docker"
		  arg0 := "stop"
		  arg1 := "dblab_server"
		  cmd := exec.Command(app, arg0, arg1)
		  _, err = cmd.CombinedOutput()
		  check(err)
		  arg0 = "start"
		  cmd = exec.Command(app, arg0, arg1)
		  _, err = cmd.CombinedOutput()
		  check(err)
                  msg := tgbotapi.NewMessage(update.Message.Chat.ID,`A: `+_resp)
                  msg.ReplyToMessageID = update.Message.MessageID
                  bot.Send(msg)
                }
                pattern = `^/clone_update\ .*$`
                matched, err = regexp.Match(pattern, []byte(update.Message.Text))
                check(err)
                if matched {
			_json := `{}`
			_path := `/clone/`+s[1]
                  _resp,err := send_to_dblab(_json,`PATCH`,_path)
                  check(err)
                  msg := tgbotapi.NewMessage(update.Message.Chat.ID,`A: `+_resp)
                  msg.ReplyToMessageID = update.Message.MessageID
                  bot.Send(msg)
                }
                pattern = `^/clone_reset\ .*$`
                matched, err = regexp.Match(pattern, []byte(update.Message.Text))
                check(err)
                if matched {
			_json := `{}`
			_path := `/clone/`+s[1]+`/reset`
                  _resp,err := send_to_dblab(_json,`POST`,_path)
                  check(err)
                  msg := tgbotapi.NewMessage(update.Message.Chat.ID,`A: `+_resp)
                  msg.ReplyToMessageID = update.Message.MessageID
                  bot.Send(msg)
                }
		pattern = `^/(help|start)$`
		matched, err = regexp.Match(pattern, []byte(update.Message.Text))
		check(err)
		if matched {
			_resp := `
			Supported commands:
			status - show info about dblab server 
			clone <dbname or any name> <id of /snapshots command> - create clone (min 1 params)
			shapshots - list of shapshots
			clone_status <name of clone> - status of clone
			clone_update <name of clone> - update time of live clone
			clone_delete <name of clone> - delete clone and restart dblab_server
			`
			msg := tgbotapi.NewMessage(update.Message.Chat.ID,`A: `+_resp)
			msg.ReplyToMessageID = update.Message.MessageID
			bot.Send(msg)
		}

	}

}

